package com.gl.dsa.GradedProject2;
import java.util.*;
public class Skyscrapper{
  public static void main(String args[]) {
    Scanner sc = new Scanner(System.in);    
    System.out.println("Enter the total number floors in the building : ");
    int n = sc.nextInt();
    Queue<Integer> s = new PriorityQueue<Integer>(n, Collections.reverseOrder());
    int x[] = new int[n + 1];
    for (int i = 1; i < n + 1; i++) {
      System.out.println("Enter the floor size given on day : " + i);
      int m = sc.nextInt();
      x[i]=m;
    }
    sc.close();
    int d = n;
    System.out.println("The order of construction is as follows ");
    int i = 1;
    int z = 1;
    while(i<=n) {    	 
    	 System.out.println("Day: " + i);
    	if(x[z]==d) {
    		s.add(x[z]);
    		z++;
    		while(!s.isEmpty() && s.peek() >=d) {
    			System.out.print(s.remove()+ " ");
    			d--;
    		}  				
    	}else{
    			s.add(x[z]);
    			z++;
    		}    		
    	System.out.println();    	
    	i++;    	
  }    
}
}
